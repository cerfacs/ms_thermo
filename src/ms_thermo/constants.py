"""
Module Holding thermodynamic constants.
"""
__all__ = ["GAS_CST", "ATOMIC_WEIGHTS"]

GAS_CST = 8.3143e0
r"""
**ATOMIC_WEIGHTS** used only in the case of a Chemkin or a Cantera Database, \
to compute molecular weight of a species *k* from its elemental composition as 

.. math:: W_k = \sum_{i}^{N_{elements}} b_{i,k} w_i 

where:

- **Wk** :  species *k* molecular weight
- **b(i,k)** : the number of element *i* atoms in species *k*
- **wi** : atomic weight of atom *i*
"""
ATOMIC_WEIGHTS = {
    "h": 1.008000e-03,
    "he": 4.003000e-03,
    "li": 6.941000e-03,
    "be": 9.012000e-03,
    "b": 1.081100e-02,
    "c": 1.201100e-02,
    "n": 1.400700e-02,
    "o": 1.599900e-02,
    "f": 1.899800e-02,
    "ne": 2.018000e-02,
    "na": 2.299000e-02,
    "mg": 2.430500e-02,
    "al": 2.698200e-02,
    "si": 2.808600e-02,
    "p": 3.097400e-02,
    "s": 3.206500e-02,
    "cl": 3.545300e-02,
    "ar": 3.994800e-02,
    "k": 3.909800e-02,
    "ca": 4.007800e-02,
    "sc": 4.495600e-02,
    "ti": 4.786700e-02,
    "v": 5.094200e-02,
    "cr": 5.199600e-02,
    "mn": 5.493800e-02,
    "fe": 5.584500e-02,
    "co": 5.893300e-02,
    "ni": 5.869300e-02,
    "cu": 6.354600e-02,
    "zn": 6.539000e-02,
    "ga": 6.972300e-02,
    "ge": 7.264000e-02,
    "as": 7.492200e-02,
    "se": 7.896000e-02,
    "br": 7.990400e-02,
    "kr": 8.380000e-02,
    "rb": 8.546800e-02,
    "sr": 8.762000e-02,
    "y": 8.890600e-02,
    "zr": 9.122400e-02,
    "nb": 9.290600e-02,
    "mo": 9.594000e-02,
    "tc": 9.800000e-02,
    "ru": 1.010700e-01,
    "rh": 1.029060e-01,
    "pd": 1.064200e-01,
    "ag": 1.078680e-01,
    "cd": 1.124110e-01,
    "in": 1.148180e-01,
    "sn": 1.187100e-01,
    "sb": 1.217600e-01,
    "te": 1.276000e-01,
    "i": 1.269050e-01,
    "xe": 1.312930e-01,
    "cs": 1.329060e-01,
    "ba": 1.373270e-01,
    "la": 1.389060e-01,
    "ce": 1.401160e-01,
    "pr": 1.409080e-01,
    "nd": 1.442400e-01,
    "pm": 1.450000e-01,
    "sm": 1.503600e-01,
    "eu": 1.519640e-01,
    "gd": 1.572500e-01,
    "tb": 1.589250e-01,
    "dy": 1.625000e-01,
    "ho": 1.649300e-01,
    "er": 1.672590e-01,
    "tm": 1.689340e-01,
    "yb": 1.730400e-01,
    "lu": 1.749670e-01,
    "hf": 1.784900e-01,
    "ta": 1.809480e-01,
    "w": 1.838400e-01,
    "re": 1.862070e-01,
    "os": 1.902300e-01,
    "ir": 1.922170e-01,
    "pt": 1.950780e-01,
    "au": 1.969670e-01,
    "hg": 2.005900e-01,
    "tl": 2.043830e-01,
    "pb": 2.072000e-01,
    "bi": 2.089800e-01,
    "po": 2.090000e-01,
    "at": 2.100000e-01,
    "rn": 2.220000e-01,
    "fr": 2.230000e-01,
    "ra": 2.260000e-01,
    "ac": 2.270000e-01,
    "th": 2.320380e-01,
    "pa": 2.310360e-01,
    "u": 2.380290e-01,
    "np": 2.370000e-01,
    "pu": 2.440000e-01,
    "am": 2.430000e-01,
    "cm": 2.470000e-01,
    "bk": 2.470000e-01,
    "cf": 2.510000e-01,
    "es": 2.520000e-01,
    "fm": 2.570000e-01,
    "md": 2.580000e-01,
    "no": 2.590000e-01,
    "lr": 2.620000e-01,
    "rf": 2.610000e-01,
    "db": 2.620000e-01,
    "sg": 2.660000e-01,
    "bh": 2.640000e-01,
    "hs": 2.770000e-01,
    "mt": 2.680000e-01,
}
