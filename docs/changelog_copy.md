# Changelog

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/).


## [0.3.5] 2022 / 06 / 27


### Changed

- Documentation of state object (V. XING)

### Fixed

- Rework temperature setter (faster, Work of V. XING)

## [0.3.4] 2022 / 03 / 11

### Fixed

- remove limitation on h5py version

## [0.3.3] 2022 / 03 / 11

### Fixed

- remove limitation on numpy version
- simpler tests , no more tightly coupled with examples folder

## [0.3.2] 2021 / 12 / 06

### Changed

- `hdfdict` from `h5cross` now

## [0.3.1] 2021 / 06 / 30

### Added

[ - ]

### Changed

- Empty __init__.py

### Fixed

- move INPUT into /src/msthermo The former structure was raisin a deprecation warning for each use of ms-thermo.
- correction in tadia cantera

### Deprecated

[ - ]

## [0.3.0] 2021 / 05 / 10

### Added
  
- state.mach, state.temperature_total,state.pressure_total,  
  
### Changed
  
[ - ]

### Fixed

- state.C_v optimized to reduce temperature computation time

### Deprecated

[ - ]

## [0.2.0] 2021 / 05 / 10

### Added
  
  - species[k].c_p, species[k].c_v, and species[k].gamma in Species
  - state.c_p, state.c_v , state.gamma, state.csound in state

### Changed
  
  - state.list_spec is preferred to state.list_species()
  - state.mix_w is preferred to state.mix_molecular_weight()

### Fixed
 [ - ]

### Deprecated

- state.list_species()
- state.mix_molecular_weight()


## [0.1.0] 2020 / 04 / 23

### Added
 - MixtureState and SpeciesState classes
 - A Gasout Tool with cli

### Changed
 - documentation
 	
### Fixed
 - tests

### Deprecated
 [ - ]

## [0.0.2] 2020 / 03 / 09
 
### Added
 - Documentation for yk_from_phi

### Changed
 - Order of parameters in cli command for yk_from_phi to match calling order in function 
 	
### Fixed
 [ - ]

### Deprecated
[ - ]


## [0.0.1] 2020 / 02 / 28
 
### Added
[ - ]

### Changed
 - __init__.py
 - README.md: More explicit documentation
 	
### Fixed
 [ - ]

### Deprecated
[ - ]



## [0.0.0] 2020 / 02 / 27
 
### Added
 - State module
 - Add tadia
 - Add yk_from_phi
 - Add flame_params
 - Add fresh_gas
 

### Changed
[ - ]
 	
### Fixed
 [ - ]

### Deprecated
[ - ]
