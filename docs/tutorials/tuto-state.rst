Using a ``State`` object
==============================================

Basic usage
********************

In ms-thermo, ``State`` objects are used to represent the full thermodynamic state of a gas mixture.
In the following example, we create a State from 10 values of temperature, pressure and species mass fractions,
then modify the temperature in part of the field.

.. code-block:: python

    import numpy as np
    from ms_thermo.state import State

    print("\nInitialize a 600 K air mixture on 10 locations")
    state = State(temperature=600. * np.ones(10),
                  pressure=100000.* np.ones(10),
                  mass_fractions_dict={'O2': 0.2325 * np.ones(10),
                                       'N2': 0.7675 * np.ones(10)}
                  )
    print(state)
    print("\nSet half of the field to 1200 K.")
    state.temperature = [600., 600., 600., 600., 600., 1200., 1200., 1200., 1200., 1200.]
    print(state)


.. code-block:: none

    Initialize a 600 K air mixture on 10 locations

    Current primitive state of the mixture
                    | Most Common |    Min    |    Max
    ----------------------------------------------------
                 rho| 5.78297e-01 | 5.783e-01 | 5.783e-01
              energy| 4.38546e+05 | 4.385e+05 | 4.385e+05
         temperature| 6.00000e+02 | 6.000e+02 | 6.000e+02
            pressure| 1.00000e+05 | 1.000e+05 | 1.000e+05
                Y_O2| 2.32500e-01 | 2.325e-01 | 2.325e-01
                Y_N2| 7.67500e-01 | 7.675e-01 | 7.675e-01


    Set half of the field to 1200 K.

    Current primitive state of the mixture
                    | Most Common |    Min    |    Max
    ----------------------------------------------------
                 rho| 2.89148e-01 | 2.891e-01 | 5.783e-01
              energy| 4.38546e+05 | 4.385e+05 | 9.411e+05
         temperature| 6.00000e+02 | 6.000e+02 | 1.200e+03
            pressure| 1.00000e+05 | 1.000e+05 | 1.000e+05
                Y_O2| 2.32500e-01 | 2.325e-01 | 2.325e-01
                Y_N2| 7.67500e-01 | 7.675e-01 | 7.675e-01

Note that the modification of the maximum temperature has also affected the minimum density.
Setting new values for the temperature also affects the density to ensure that the equation of state is satisfied.
Explanations on the inner workings of a ``State`` are given :ref:`here <Representing mixtures with State objects>`.


Computing thermodynamic quantities in post-processing
*****************************************************

``States`` can be useful to easily recover any thermodynamic quantity from the base set of primitive or conservative variables.
In the following example, the temperature and heat capacity at constant pressure in an AVBP solution are computed and saved using ms-thermo.

.. code-block:: python

    from h5cross import hdfdict
    import h5py
    import numpy as np
    from ms_thermo.state import State

    sol_path = '1DFLAME/solut_00000050.h5'

    with h5py.File(sol_path, "r") as fin:
        sol = hdfdict.load(fin, lazy=False)
    state = State.from_cons(
        sol["GaseousPhase"]["rho"], sol["GaseousPhase"]["rhoE"], sol["RhoSpecies"]
    )
    np.save('T.npy', state.temperature)
    np.save('Cp.npy', state.c_p)
