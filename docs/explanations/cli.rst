Command line tools
====================================

Terminal commands are available for common use cases.

``gasout``
########################

This command is a pre-processing tool built on top of the ms-thermo package.
It modifies the state of a mixture in specific regions of the domain.
This is the default input file demonstrating the actions that are available.

.. code-block:: yaml

    # Input files
    inst_solut: ./solut_0003.sol.h5
    mesh: ./solut_0003.mesh.h5
    inst_solut_output: ./gasouted.sol.h5

    # Actions to take
    actions:
    - type: directional_linear_mask
      direction: "x"           # Direction x, y, or z
      transition_start: 0.1
      transition_end: 0.11
      new_pressure: null       # Use null if you don't want to edit this field
      new_temperature: 2000.0  # Temperature in K
      new_yk: null

    - type: spherical_tanh_mask
      center: [0.1, 0.1, 0]    # Center of the sphere
      radius: 0.01             # Radius of the sphere [m]
      delta: 0.05              # Transition thickness [m]
      new_pressure: null
      new_temperature: 1600.
      new_yk:                  # Dictionary of species Yk
        N2: 0.8                # Order does not matter
        O2: 0.2                # Sum MUST be 1!

    - type: fraction_z_mask
      specfuel: KERO_LUCHE     # Fuel species name
      atom_ref: C              # Reference atom for Z computation (default: 'C')
      oxyd_mass_fracs:         # Mixture oxidizer for Z computation (default: AIR)
        O2: 0.233
        N2: 0.767
      fuel_mass_fracs:  None   # Mixture fuel (default: mixture at peak fuel concentration)
      zmax: 0.02
      zmin: 0.01
      new_pressure: 103000.0   # Pressure in Pa
      new_temperature: null
      new_yk: null

    - type: fraction_z_mask    # You can repeat several treatments
      specfuel: KERO_LUCHE
      atom_ref: C
      oxyd_mass_fracs:
        O2: 0.233
        N2: 0.767
      fuel_mass_fracs:  None
      zmax: 0.05
      zmin: 0.04
      new_pressure: 103000.0
      new_temperature: null
      new_yk: null

This default input file is generated when calling ``ms_thermo gasout --new foo.yml``.
From an input YAML file, run the tool with ``ms_thermo gasout foo.yml``.


``hp-equil``
########################

Compute the adiabatic flame temperature and mass fractions using a CANTERA `.cti` file.

.. code-block:: none

    >ms_thermo hp-equil 300 101325 1 NC10H22 10 22 src/ms_thermo/INPUT/Luche1.cti

    The adiabatic flame temperature of a mix NC10H22-air from cantera is : 2277.42 K.

    Species     |    Mass fraction
    ------------------------------
    CO          |       0.014
    CO2         |       0.172
    H2O         |       0.084
    N2          |       0.718
    NO          |       0.003
    O2          |       0.007
    OH          |       0.002
    + 82 others |       0.000

.. warning:: This command requires the `Cantera <https://cantera.org/>`_ package.


``kero-prim2cons``
########################

Compute the conservative variables of a kerosene-air mixture from primitive variables T, P and phi (equivalence ratio).

.. code-block:: none

    >ms_thermo kero-prim2cons 300 101325 1

    rho       |  1.232 kg/m3
    rhoE      |  266054.682 J.kg/m3
    rhoYk     |
     KERO     |  0.077 mol.kg/m3
     N2       |  0.886 mol.kg/m3
     O2       |  0.269 mol.kg/m3
    ------------------------------
    Yk        |
     KERO     |  0.063 [-]
     N2       |  0.719 [-]
     O2       |  0.218 [-]


``kero-tadia``
########################

Compute the final adiabatic temperature of a kerosene-air mixture from T, P and phi (equivalence ratio)
It is based on tabulation created using `Cantera <https://cantera.org/>`_.

.. code-block:: none

    >ms_thermo kero-tadia 300 101325 0.7

    The adiabatic flame temperature of a mix C10H22-air from tables is : 1904.30 K.

    Species     |    Mass fraction
    ------------------------------
    N2          |       0.732
    KERO        |       0.000
    O2          |       0.067
    CO2         |       0.143
    H2O         |       0.058


``yk-from-phi``
########################

Compute species mass fractions of a hydrocarbon fuel-air mixture. Inputs are the equivalence ratio and the numbers of C and H atoms in the fuel.

.. code-block:: none

    >ms_thermo yk-from-phi 0.7 1 4 CH4

    Species     |    Mass fraction
    ------------------------------
    CH4         |       0.078
    N2          |       0.707
    O2          |       0.215
