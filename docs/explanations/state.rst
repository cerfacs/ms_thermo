Representing mixtures with State objects
=============================================

The ``State`` object provides a full picture of the themochemical state of a gaseous
mixture. It can be used to extract missing thermochemical quantities, or to keep a
meaningful thermochemical state while modifying individual state variables. A ``State``
does not contain any velocity or numerical information.

``State`` initialization
########################

A ``State`` object can be initialized in two ways:

- From the temperature, pressure, and species mass fractions :math:`(T, P, Y_k)` through the default constructor::

    state = State(T, P, Yk)

- From conservative variables :math:`(\rho, \rho E, \rho Y_k)` through the ``from_cons`` constructor::

    state = State.from_cons(rho, rhoE, rhoYk)

:math:`E` is the total non chemical energy defined as the sum of the sensible and kinetic energies ([TNC]_ Table 1.4)

The constructor arguments :math:`(T, P, Y_k)` or :math:`(\rho, \rho E, \rho Y_k)` can be scalars or multidimensional arrays.

.. warning::
    When initializing from conservative variables, :math:`T` is determined by a
    Newton-Raphson method to ensure that the mixture energy matches the input energy.
    This is an **expensive** step that may take a long time for large inputs.

The following flowchart details how ``from_cons`` works

.. figure:: ../_images/fromcons_flowchart.png
    :align: center
    :width: 600

|

``State`` transformations
#########################

After a ``State`` has been initialized, :math:`T`, :math:`P` and :math:`Y_k` can
independently be set to new values (`e.g.` ``myState.temperature = newTemperature``) and the other state variables are modified
accordingly:

- When setting a new value for :math:`T`, the other state variables are modified assuming an **isobaric and iso-composition** transformation from the previous state.
- When setting a new value for :math:`P`, the other state variables are modified assuming an **isothermal and iso-composition** transformation from the previous state.
- When setting a new value for :math:`Y_k`, the other state variables are modified assuming an **isothermal and isobaric** transformation from the previous state.

State transformations always satisfy the perfect gas equation of state

.. math:: P = \rho \frac{R}{W_{mix}} T

.. [TNC] Poinsot, T.; Veynante, D. Theoretical and Numerical Combustion, 3rd ed.; 2011.
