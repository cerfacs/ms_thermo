Explanations
=============

Presentation of the key features of ms\_thermo.

.. toctree::
    :maxdepth: 2

    state.rst
    cli.rst
