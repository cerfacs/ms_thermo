.. Tekigo documentation master file, created by
   sphinx-quickstart on Mon Jan 20 13:57:27 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: _images/newCerfacs.png
    :alt: cerfacs_log

|

Welcome to the ms\-thermo documentation!
===========================================

This is a small package from Cerfacs dedicated to multispecies thermodynamics operations.
It offers a simple way to manipulate reactive multispecies mixtures through :ref:`State objects <Representing mixtures with State objects>`.
:ref:`Command line tools <Command line tools>` are also available for a few common tasks.
:ref:`Tutorials <Tutorials>` and more advanced :ref:`how-to guides <How-to Guides>` provide sample use cases for pre-processing and post-processing typical CFD multispecies mixtures.


Contributors
------------

This Python package is currently being developped by the COOP team at Cerfacs, with a non exhaustive list of the main contributors as of June 2022:
Antoine Dauptain, Aimad Er-Raiy, Matthieu Rossi, Théo Defontaine, Thibault Gioud, Thibault Duranton, Elsa Gullaud, Victor Xing

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorials/tutorials
   howto/howto
   explanations/explanations
   changelog_copy
   api/ms_thermo
