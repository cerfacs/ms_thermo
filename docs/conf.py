# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import shutil


def _get_version():
    version = ""
    release = ''
    with open('../setup.py', "r") as fin:
        for line in fin.readlines():
            if all([key.lower() in line for key in ["VERSION", "=", "."]]):
                ver = line.split('=')[-1].replace('"', '').replace("'", "").strip()
                ver_split = [key for key in ver.split('.') if key]
                if len(ver_split) >= 3:
                    release = ".".join(ver.split('.')[:3])
                    version = ".".join(ver.split('.')[:2])
                elif len(ver_split) == 2:
                    release = "%s.0" % ver
                    version = ver
                elif len(ver_split) == 1:
                    release = "%s.0.0" % ver.replace('.', '')
                    version = "%s.0" % ver.replace('.', '')
                break
    return release, version

def _copy_changelog():
    print("copying changelog")
    shutil.copy('../CHANGELOG.md', './changelog_copy.md')

_copy_changelog()

# -- Project information -----------------------------------------------------
import sphinx_rtd_theme

project = 'MS Thermo'
copyright = '2022, COOP Team'
author = 'COOP Team'

# The full version, coming from setup.py
release, version = _get_version()


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = ['sphinx.ext.autodoc',
              'sphinx.ext.autosectionlabel',
              'myst_parser']

autosectionlabel_maxdepth = 2  # Only generate section anchors for top-level mk sections

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

master_doc = 'index'
source_suffix = ['.rst', '.md']
# autodoc_mock_imports = ['example.py', 'complex_example.py']

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'sphinx_rtd_theme'

# # Add any paths that contain custom static files (such as style sheets) here,
# # relative to this directory. They are copied after the builtin static files,
# # so a file named "default.css" will overwrite the builtin "default.css".
# # Options for sphinxcontrib.apidoc
# # The path to the module to document. This must be a path to a Python package.
# # This path can be a path relative to the documentation source directory or an
# # absolute path.
# # Required.
# apidoc_module_dir = "../src/tekigo"

# # The output directory. If it does not exist, it is created. This path is
# # relative to the documentation source directory.
# # Optional. Default to "api".
# apidoc_output_dir = "api"

# # Put documentation for each module on its own page. Otherwise there will be one
# # page per (sub)package.
# # Optional. Default to False.
# apidoc_separate_modules = False

# # When set to True, put module documentation before submodule documentation.
# # Optional. Default to False.
# apidoc_module_first = True

# apidoc_toc_file = False

# apidoc_excluded_paths = ['tests', '*example*', '_*']
