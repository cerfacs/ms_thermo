# Contributing

If you are here, it means you wish to help this project go forward. So first:
**Thanks!**

## The Python APIs

The engine is in the `src/ms_thermo` folder. Have fun :).

Please create reasonably sized branches around a clear feature `X`, and
call it `feature_X`. Before pushing, please follow these instructions:

  - the first time: run `make init` (installs the dependencies)
  - then, before each commit, run:
      - `make lint`: checks your syntax. You **must** get 10/10 on this step.
      - `make test`: checks that all unit tests pass. Again, less than
         100% is not acceptable for merging.

When you have passed all both the above tests, push your branches to the
master repo and create a merge request towards `master`.

For more information, please see COOP team website ressources about developping 
and software versioning : [developping](http://cerfacs.fr/coop/links/), 
[software versioning](http://cerfacs.fr/coop/versioning/)

## Thanks for contributing!
